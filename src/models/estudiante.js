const { Schema, model } = require('mongoose');

const Estudiante = new Schema({
    N: {
        type: Boolean,
        default: false
     },
    nombre: {type: String, required: true },
    monto: {type: Number, default: 0 }
});
module.exports = model('Estudiante', Estudiante);