const express = require('express');
const path = require('path');
const engine = require('ejs-mate');

const app = express();
require('./db');

//settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', engine);
app.set('view engine', 'ejs');

//mi

app.use(express.urlencoded({extended: false}));
app.use(express.json());

//routes
app.use(require('./routes/index'));

//static files
app.use(express.static(path.join(__dirname, 'public')));
//start the
app.listen(app.get('port'), () => {
      console.log(`Server port ${app.get('port')}`);
});