const { Router } = require('express');
const router = Router();

const Estudiante = require('../models/estudiante');

router.get('/', async (req, res) => {
    const estudiantes = await Estudiante.find();
    console.log(estudiantes)
    res.render('index', {estudiantes: estudiantes});
});
router.post('/registrar', async (req, res) => {
    const newEstudiante = new Estudiante(req.body);
    await newEstudiante.save();
    res.redirect('/');
});
router.get('/turn/:id', async (req, res, next) => {
    let { id } = req.params;
    const estudiantes = await Estudiante.findById(id);
    estudiantes.N = !estudiantes.N;
    await estudiantes.save();
    res.redirect('/');
  });
router.get('/edit/:id', async (req, res, next) => {
    const estudiantes = await Estudiante.findById(req.params.id);
    console.log(estudiantes)
    res.render('edit', { estudiantes });
});
router.post('/edit/:id', async (req, res, next) => {
    const { id } = req.params;
    await Estudiante.update({_id: id}, req.body);
    res.redirect('/');
});
module.exports = router;