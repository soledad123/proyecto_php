const mongoose = require('mongoose');

mongoose.connect('mongodb://172.17.0.3/order-list', {
    useNewUrlParser: true
})
       .then(db => console.log('DB is connected'))
       .catch(err => console.log(err));